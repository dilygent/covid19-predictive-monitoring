# Monitoreo predictivo de la COVID-19

Aplicación web que presenta estadística y proyecciones basadas en modelos matemáticos. Actualmente publicada en: https://covid19.dilygent.com/
